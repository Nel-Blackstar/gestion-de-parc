<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipement;

class equipementsController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function choix()
  {
      return view('equipements.choix');
  }

  public function index()
  {
      return view('equipements.index');
  }

  public function show()
  {
      $equipements=Equipement::all();
      return view('equipements.liste')->with('equipements',$equipements);
  }

  public function store(Request $request)
  {
        Equipement::create($request->all());
      return redirect()->back()->with("infos", "Enregistrement effectuer avec success");
  }

  public function signaler()
  {
      return view('equipements.signaler');
  }
}
