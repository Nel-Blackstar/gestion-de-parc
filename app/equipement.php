<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class equipement extends Model
{
  protected $fillable = ['serial', 'marque','type','image','proprietaire' ,'description'];
  protected $guarded = ['id', 'created_at'];

  public function getId() {
      return $this->id;
  }
}
