<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/choix','equipementsController@choix')->name('choix-equip');
Route::get('/ajouter_equipement','equipementsController@index')->name('ajouter_equipement');
Route::post('/ajouter_equipement','equipementsController@store')->name('store_equipement');
Route::get('/lister_equipement','equipementsController@show')->name('lister_equipement');
Route::get('/signaler_equipement','equipementsController@signaler')->name('signaler_equipement');
