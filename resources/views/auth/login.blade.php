@extends('layouts.authTemplate')

@section("titre")
  Connectez - Vous
@endsection
@section('contenue')

  @section('action-page')
      {{ Route('login') }}
  @endsection

  @include('partials.form-input',[
    "label" => "Addresse E-Mail",
    "name" => "email",
    "type" => "email",
    "required" => true,
    "autofocus" => true,
    "icon" => "email"
  ])

  @include('partials.form-input',[
    "label" => "Mot de Passe",
    "name" => "password",
    "type" => "password",
    "required" => true,
    "icon" => "lock"
  ])
  <p>
      <label>
          <input type="checkbox" class="filled-in"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
        <span>Se Souvenir de moi</span>
        </label>
    </p>
@endsection

@section('action')
  <div class="center-align">
      @include('partials.button-submit',["text" => "Connexion"])

      @if (Route::has('password.request'))
          <a class="btn btn-flat red white-text waves-effect waves-light" href="{{ route('password.request') }}">
              {{ __('Mot de passe oubllier?') }}
          </a>
      @endif
  </div>
@endsection
