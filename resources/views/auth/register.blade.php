@extends('layouts.authTemplate')

@section("titre")
  S'inscrire
@endsection

@section('action-page')
    {{ Route('register') }}
@endsection

@section('contenue')
  @include('partials.form-input',[
    "label" => "Nom",
    "name" => "name",
    "type" => "text",
    "required" => true,
    "autofocus" => true,
    "icon" => "assignment_ind"
  ])

  @include('partials.form-input',[
    "label" => "Addresse E-Mail",
    "name" => "email",
    "type" => "email",
    "required" => true,
    "icon" => "email"
  ])
  @include('partials.form-input',[
    "label" => "Mot de Passe",
    "name" => "password",
    "type" => "password",
    "required" => true,
    "icon" => "lock"
  ])
  @include('partials.form-input',[
    "label" => "Mot de Passe",
    "name" => "password_confirmation",
    "type" => "password",
    "required" => true,
    "icon" => "lock"
  ])
@endsection

@section('action')
  <div class="center-align">
      @include('partials.button-submit',["text" => "S'inscrire"])
      @include('partials.button-reset',["text" => "Annuler"])
  </div>
@endsection
