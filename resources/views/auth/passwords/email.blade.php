@extends('layouts.authTemplate')

@section("titre")
Reinitialiser Le Mot de Passe
@endsection
@section('contenue')

  @section('action-page')
      {{ Route('password.email') }}
  @endsection

  @include('partials.form-input',[
    "label" => "Addresse E-Mail",
    "name" => "email",
    "type" => "email",
    "required" => true,
    "autofocus" => true,
    "icon" => "email"
  ])
@endsection

@section('action')
  <div class="center-align">
      @include('partials.button-submit',["text" => "Envoyer l'Email de confirmation"])
      @include('partials.button-reset',["text" => "Annuler"])
  </div>
@endsection
