@extends('layouts.authTemplate')

@section("titre")
Entrez Le Nouveau Mot de Passe
@endsection
@section('contenue')

  @section('action-page')
      {{ Route('password.update') }}
  @endsection

  @include('partials.form-input',[
    "label" => "Addresse E-Mail",
    "name" => "email",
    "type" => "email",
    "required" => true,
    "autofocus" => true,
    "icon" => "email"
  ])

  @include('partials.form-input',[
    "label" => "Mot de Passe",
    "name" => "password",
    "type" => "password",
    "required" => true,
    "icon" => "lock"
  ])

  @include('partials.form-input',[
    "label" => "Confirmer le Mot de Passe",
    "name" => "password_confirmation",
    "type" => "password",
    "required" => true,
    "icon" => "lock"
  ])
@endsection

@section('action')
  <div class="center-align">
      @include('partials.button-submit',["text" => "Reinitialiser Mon Mot de passe"])
      @include('partials.button-submit',["text" => "Annuler"])
  </div>
@endsection
