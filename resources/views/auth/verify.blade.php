@extends('layouts.authTemplate')

@section("titre")
{{ __('Verifier votre adresse E-mail') }}
@endsection
@section('contenue')
  @if (session('resent'))
  <div class="row">
    <div class="col s12">
      <div class="card-panel green darken-3">
        <span class="white-text">
          Un nouveau lien de confirmation vous a ete envoyer
        </span>
      </div>
    </div>
  </div>
  @endif

  <h4>{{ __('Verifier vos email avant de continuer') }}</h4>
@endsection

@section('action')
  <div class="center-align">
        {{ __('Vous n\'avez pas recu l\'Email??') }}, <a href="{{ route('verification.resend') }}" class="btn">{{ __('Envoyer de nouveau') }}</a>.
  </div>
@endsection
