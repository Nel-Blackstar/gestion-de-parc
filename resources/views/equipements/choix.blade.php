@extends('layouts.app')

@section("page-navbar")
@include("partials.navbar")
@endsection


@section("content")
<div class="container main-container">
<div class="row"  style="justify-content:center;">
<div class="col s12 l3 offset-l1">
  <div class="card small hoverable">
      <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src="{{asset('images/equipement.png')}}">
      </div>
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">Ajouter<i class="material-icons right">more_vert</i></span>
        <p><a href="{{ Route('ajouter_equipement') }}">Cliquer Ici !!</a></p>
      </div>
      <div class="card-reveal">
        <span class="card-title grey-text text-darken-4">Ajouter<i class="material-icons right">close</i></span>
        <p>Vous serrez rediriger vers le formulaire permettant d'ajouter des equipements.</p>
      </div>
    </div>
</div>
<div class="col s12 l4  ">
  <div class="card small hoverable">
      <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src="{{asset('images/entrepot.png')}}">
      </div>
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">Lister<i class="material-icons right">more_vert</i></span>
        <p><a href="{{ Route('lister_equipement') }}" class="green-text">Cliquer Ici !!</a></p>
      </div>
      <div class="card-reveal">
        <span class="card-title grey-text text-darken-4">Lister<i class="material-icons right">close</i></span>
        <p>Vous serrez rediriger vers la page listant l ensemble des equipements enregistrer</p>
      </div>
    </div>
</div>

<div class="col s12 l3  ">
  <div class="card small hoverable">
      <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src="{{asset('images/assistant.png')}}">
      </div>
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">Signaler<i class="material-icons right">more_vert</i></span>
        <p><a href="{{Route('signaler_equipement')}}" class="red-text">Cliquer Ici !!</a></p>
      </div>
      <div class="card-reveal">
        <span class="card-title grey-text text-darken-4">Signaler<i class="material-icons right">close</i></span>
        <p>Vous serrez rediriger vers le formulaire permettant de signaler des pannes</p>
      </div>
    </div>
</div>
</div>
</div>
@endsection

@section("page-footer")
  @include('partials.footer')
@endsection
