@extends('layouts.authTemplate')

@section("titre")
  Signalez Une panne
@endsection
@section('contenue')

  @section('action-page')
      {{ Route('login') }}
  @endsection

  @include('partials.form-input',[
    "label" => "Numero de serie",
    "name" => "serial",
    "type" => "text",
    "required" => true,
    "autofocus" => true,
  ])
  <div class="input-field">
    <textarea id="textarea1" class="materialize-textarea" name="description"></textarea>
    <label for="textarea1">Decrivez la panne</label>
  </div>
@endsection

@section('action')
  <div class="center-align">
      @include('partials.button-submit',["text" => "Enregistrez"])
      @include('partials.button-reset',["text" => "Annuler"])
  </div>
@endsection
