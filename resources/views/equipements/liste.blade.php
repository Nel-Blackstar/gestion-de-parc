@extends('layouts.app')

@section("page-navbar")
@include("partials.navbar")
@endsection


@section("content")
<div class="container">
    <div class="row justify-content-center">
      <div class="col s12">
  <h3 class="header">Liste des Equipements</h3>
@foreach($equipements as $equipement)
  <div class="card horizontal">
    <div class="card-image">
      <img src="{{asset('images/users/back2.jpeg')}}" style="width:14em;">
    </div>
    <div class="card-stacked">
      <div class="card-content">
        <table class="striped">
          <tr>
            <td><b>Numero de serie : </b></td><td>{{$equipement->serial }}</td><td><b>Proprietaire : </b></td><td>{{$equipement->proprietaire}}</td>
          </tr>
          <tr>
            <td><b>TYPE : </b></td><td>{{$equipement->type}}</td><td><b>Marque : </b></td><td>{{$equipement->marque}}</td>
          </tr>
          <tr>
            <td><b>Description</b></td><td colspan="3">{{$equipement->description}}</td>
          </tr>
        </table>
      </div>
      <div class="card-action">
        <a href="#" class="btn red right"><i class="material-icons">delete</i></a>
        <a href="#" class="btn orange right mr-2"><i class="material-icons">edit</i></a>
      </div>
    </div>
  </div>
@endforeach
</div>
  </div>
</div>
@endsection

@section("page-footer")
  @include('partials.footer')
@endsection
