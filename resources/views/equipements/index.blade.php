@extends('layouts.authTemplate')

@section("titre")
Enregistrez Un Equipement
@endsection

@section('action-page')
    {{ Route('store_equipement') }}
@endsection

@section('contenue')
  @include('partials.form-input',[
    "label" => "Numero de serie",
    "name" => "serial",
    "type" => "text",
    "required" => true,
  ])

  @include('partials.form-input',[
    "label" => "Marque",
    "name" => "marque",
    "type" => "text",
    "required" => true,
    "autofocus" => true,
  ])
  <div class="input-field">
    <select id="type" name="type">
      <optgroup label="Ordinateur">
        <option value="1">LAPTOP</option>
        <option value="2">DESKTOP</option>
      </optgroup>
      <optgroup label="Imprimante">
        <option value="3">LAZER</option>
        <option value="4">JET D'ANCRE</option>
      </optgroup>
      <optgroup label="Equipement Reseau">
        <option value="3">MODEM</option>
        <option value="4">ROUTEUR</option>
      </optgroup>
    </select>
    <label for="type">Type</label>
  </div>

        <div class="input-field">
          <textarea id="textarea1" class="materialize-textarea" name="description"></textarea>
          <label for="textarea1">Description</label>
        </div>

        <div class="file-field input-field">
      <div class="btn btn-small">
        <span>Image</span>
        <input type="file">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>

    @include('partials.form-input',[
      "label" => "Proprietaire",
      "name" => "proprietaire",
      "type" => "text",
      "required" => true,
    ])
@endsection

@section('action')
  <div class="center-align">
      @include('partials.button-submit',["text" => "Enregistrer"])
      @include('partials.button-reset',["text" => "Annuler"])
  </div>
@endsection
