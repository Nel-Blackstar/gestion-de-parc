<button type="reset" class="btn btn-flat red white-text waves-effect waves-light">
    {{ $text }}
</button>
