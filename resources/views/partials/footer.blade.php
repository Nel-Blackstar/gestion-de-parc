<footer class="page-footer grey darken-3">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Easy Manager</h5>
        <p class="grey-text text-lighten-4">Nous somme une equipe de devellopeur passionner et dynamique , specialiser dan s la programation WEB, nous demonttrons notre talen par de multiple
           application permettant de faciliter la vie de tout un chacun</p>
      </div>
      <div class="col l3 s12">
        <h5 class="white-text">Plus D'information</h5>
        <ul>
          <li><a class="white-text" href="#!">Pourquoi utilliser?</a></li>
          <li><a class="white-text" href="#!">Comment Utiliser?</a></li>
          <li><a class="white-text" href="#!">Contribuer</a></li>
          <li><a class="white-text" href="#!">Mention Legal</a></li>
        </ul>
      </div>
      <div class="col l3 s12">
        <h5 class="white-text">Suivez Nous</h5>
        <ul>
          <li><a class="white-text" href="#!">Facebook</a></li>
          <li><a class="white-text" href="#!">Youtube</a></li>
          <li><a class="white-text" href="#!">Instagram</a></li>
          <li><a class="white-text" href="#!">Twitter</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    Realiser par <a class="orange-text text-lighten-3" href="#">Blackstar  </a><a href="https://www.facebook.com/Nelson.blackstars" target="_blank" class='btn btn-floating pulse btn-small right lien'><i class="material-icons">comment</i></a>
    </div>
  </div>
</footer>
