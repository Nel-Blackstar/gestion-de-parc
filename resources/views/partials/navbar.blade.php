<nav class="light-blue lighten-1" role="navigation">
  <div class="nav-wrapper container"><a id="logo-container" href="{{url('home')}}" class="brand-logo">{{ config('app.name', 'PIM') }}</a>
    <ul class="right hide-on-med-and-down">
      @guest
      <li><a href="{{Route('login')}}">Connexion</a></li>
      @if (Route::has("register"))
      <li><a href="{{Route('register')}}">Inscription</a></li>
      @endif
      @else
        <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">
          <i class="material-icons left">account_circle</i>
          {{ Auth::user()->name }}
          <i class="material-icons right">arrow_drop_down</i>
        </a></li>
                  <ul id="dropdown1" class="dropdown-content">
                        <li><a href="#!"><i class="material-icons left">assignment_ind</i>Profil</a></li>
                        <li><a href="#!"><i class="material-icons left">history</i>Historique</a></li>
                        <li class="divider" tabindex="-1"></li>
                        <li><a href="#!" href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><i class="material-icons left red-text">input</i>Deconnection</a></li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @endguest

    </ul>
          <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>

<ul id="slide-out" class="sidenav">
    <li><div class="user-view">
      <div class="background">
        <img src="images/users/back2.jpeg"  width="100%" height="100%">
      </div>
      <a href="#user"><img class="circle" src="images/users/profile1.jpg" width="100px" height="100px"></a>
      <a href="#name"><span class="white-text name">Blackstar</span></a>
      <a href="#email"><span class="white-text email">Blackstar@gmail.com</span></a>
    </div></li>
    <li><a href="#!"><i class="material-icons">cloud</i>Acceuil</a></li>
    <li><a href="#!">Autre lien</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Division</a></li>
    <li><a class="waves-effect" href="#!">Parametre</a></li>
  </ul>
