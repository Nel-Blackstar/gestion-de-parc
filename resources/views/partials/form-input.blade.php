<div class="input-field">
    {!! isset($icon) ? "<i class='material-icons prefix'>$icon</i>" : '' !!}
        <input id="{{ $name }}" type="{{ $type }}" name="{{ $name }}" value="{{ old($name) }}"  {{ isset($autofocus) ? 'autofocus' : '' }}   {{ isset($required) ? 'required' : '' }}>
            <label for="{{ $name }}" >{{ $label }}</label>
        @if ($errors->has($name))
          <span class="helper-text">
                <strong class="red-text">{{ $errors->first($name) }}</strong>
            </span>
        @endif
</div>
