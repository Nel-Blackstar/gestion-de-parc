<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name') }}</title>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>{{ config('app.name') }}</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body
@isset($infos)
 onload="M.toast({html: {{$infos}}', completeCallback: function(){alert('OK')}})"
@endisset
>
<div class="main">
  @yield('page-navbar')

    @yield("content")

</div>
  <!--  Scripts-->
  @yield("page-footer")
  <script src="{{asset('js/app.js')}}"></script>
  <script src="{{asset('js/materialize.js')}}"></script>
  <script src="{{asset('js/script.js')}}"></script>

  </body>
</html>
