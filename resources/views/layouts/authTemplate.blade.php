@extends('layouts.app')

@section("page-navbar")
@include("partials.navbar")
@endsection


@section('content')
<div class="container col s12">
                    <form method="POST" action="@yield('action-page')">
                        @csrf
                          <div class="row login-form  login-form">
                              <div class="col s12 m10 l6 offset-l3 offset-m1">
                                <div class="card">
                                  <div class="card-header light-blue lighten-1">
                                        <h4 class="card-title white-text">@yield('titre')</h4>
                                  </div>
                                  <div class="card-content">
                                  @yield('contenue')
                                </div>
                                  <div class="card-action">
                                    @yield('action')
                                  </div>
                                </div>
                              </div>
                            </div>
                    </form>
</div>
@endsection

@section("page-footer")
  @include('partials.footer')
@endsection
