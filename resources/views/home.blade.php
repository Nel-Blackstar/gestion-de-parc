@extends('layouts.app')

@section("page-navbar")
@include("partials.navbar")
@endsection


@section("content")
<div class="container mt-4">
    <div class="row justify-content-center">
    <div class="col s12 m4">
      <div class="card card-black">
        <div class="card-image">
          <img src="{{asset('images/users/back2.jpeg')}}">
          <span class="card-title">Gerer Les Equipement</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red" href="{{route('choix-equip')}}"><i class="material-icons">add</i></a>
        </div>
        <div class="card-content">
          <p>L'ensemble des fonctionnaliter de gestion pour vos equipements se trouve ici! Vous pourrez enregistrer , lister et supprimer vos equipenemt ici</p>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card card-black">
        <div class="card-image">
          <img src="{{asset('images/users/back2.jpeg')}}">
          <span class="card-title">Assistance</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">perm_phone_msg</i></a>
        </div>
        <div class="card-content">
          <p>Beneficier de notre assistance grace a ce lien, vous pourrez posser ici tous vous problemes et question : notre assistance se feras un plaisir de vous repondre</p>
        </div>
      </div>
    </div>
    <div class="col s12 m4">
      <div class="card card-black">
        <div class="card-image">
          <img src="{{asset('images/users/back2.jpeg')}}">
          <span class="card-title">Atelier de Reparation</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">settings</i></a>
        </div>
        <div class="card-content">
          <p>Verifier ici les equipement enregistrer dans l'application en cour de reparation, les different ajustement effectuer ici serons imediatement pris en compte</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section("page-footer")
  @include('partials.footer')
@endsection
