@extends('layouts.app')

@section("page-navbar")
@include("partials.navbar")
@endsection


@section("content")
  <div id="index-banner" class="parallax-container">
  <div class="section no-pad-bot">
    <div class="container">
      <br><br>
      <h1 class="header center white-text text-lighten-2">Bienvenue</h1>
      <div class="row center">
        <h5 class="header col s12 light">Votre system efficace de gestion pour vous materiel informatique</h5>
      </div>
      <div class="row center">
        <a href="{{Route('login')}}" id="download-button" class="btn-large waves-effect waves-light orange">Commencer</a>
      </div>
      <br><br>
    </div>
  </div>
  <div class="parallax"><img src="{{asset("images/background.jpg")}}" alt="Unsplashed background img 1"></div>
</div>

  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">info</i></h2>
            <h5 class="center">Rapide et Simple</h5>

            <p class="light">Grace a une longue et etude sur ke terrain, nous avouns pu optimiser l'ensemble des service que propose
              Notre plate forme, vous serrez surprie par sont efficaciter</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">forum</i></h2>
            <h5 class="center">Experience d'utilisateur</h5>

            <p class="light">Depuis sa mise sur pied, Easy Manager  a reussi a captiver l'attention de plusieur Utilisateur, grace a sont design et sont decor parlans, il satisfait a la fois cnnaisseur
            et amateur , les divers avis en ligne parle D'eux même</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">desktop_windows</i></h2>
            <h5 class="center">Facile et Utile</h5>

            <p class="light">Grace au different elements de notre plate forme, il devient tres facil pour vous d'administrer la gest de vous outils informatique
            grace a nos diver formulaire et nos pages haut en design vous pourrez etre tres efficase Dans l'accomplissage de vos tache</p>
          </div>
        </div>
      </div>

    </div>
    <br><br>
  </div>
@endsection

@section("page-footer")
  @include('partials.footer')
@endsection
